import app from "./app";
import connectDb from "./utils/db/connectDb";
import { logger } from "./utils/logger/logger";

const PORT = process.env.PORT || 3005;

(async () => {
	try {
		await connectDb();
		app.listen(PORT, () => {
			logger.info(`Server started on port ${PORT}`);
		});
	} catch (error: any) {
		logger.error(`[Error] ${error.message}`);
		process.exit(1);
	}
})();