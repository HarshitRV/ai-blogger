import { Request, Response, NextFunction } from "express";
import asyncErrorHandler from "../../../utils/error_handler/asyncErrorHandler";
import { verifyToken } from "../../../utils/jwt/jwt";

const protect = asyncErrorHandler(
	async (req: Request, res: Response, next?: NextFunction) => {
		return res.status(200).json({
			message: "protect middleware is working",
		});
	}
);

export default protect;