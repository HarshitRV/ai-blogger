import mongoose, { Document, Model } from "mongoose";
import Joi, { valid } from "joi";
import bcrypt from "bcrypt";

const Schema = mongoose.Schema;

export const ROLES = {
	USER: "ROLE_USER",
	ADMIN: "ROLE_ADMIN",
} as const;

export interface IUser extends Document {
	username: string;
	email: string;
	password: string;
	role: "ROLE_USER" | "ROLE_ADMIN";
	verified: boolean;
	avatar?: string;
	otp?: string;
	checkPassword(password: string): Promise<boolean>;
	checkOTP(otp: string): Promise<boolean>;
}

export interface UserModel extends Model<IUser> {}

const opts = { toJSON: { virtuals: true }, timestamps: true };

const userSchema = new Schema(
	{
		username: {
			type: String,
			required: [true, "username is required"],
			trim: true,
			validate: {
				validator: function (v: string) {
					return (
						Joi.string()
							.pattern(/^[a-zA-Z0-9_]{3,30}$/)
							.validate(v).error === undefined
					);
				},
				message: (props: mongoose.ValidatorProps) =>
					`${props.value} is not a valid username!`,
			},
			unique: true,
		},
		email: {
			type: String,
			validate: {
				validator: function (v: string) {
					return Joi.string().email().validate(v).error === undefined;
				},
				message: (props: mongoose.ValidatorProps) =>
					`${props.value} is not a valid email address!`,
			},
			required: [true, "email is required"],
			unique: true,
			trim: true,
			lowercase: true,
		},
		password: {
			type: String,
			validate: {
				validator: function (v: string) {
					return (
						Joi.string()
							.pattern(
								new RegExp(
									"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{12,})"
								)
							)
							.validate(v).error === undefined
					);
				},
				message: (props: mongoose.ValidatorProps) =>
					`${props.value} is not a valid password!`,
			},
			required: [true, "password is required"],
			trim: true,
		},
		avatar: {
			type: String,
			validate: {
				validator: function (v: string) {
					return Joi.string().uri().validate(v).error === undefined;
				},
				message: (props: mongoose.ValidatorProps) =>
					`${props.value} is not a valid avatar URL!`,
			},
			trim: true,
		},
		role: {
			type: String,
			default: ROLES.USER,
			enum: [ROLES.USER, ROLES.ADMIN],
		},
		verified: {
			type: Boolean,
			default: false,
		},
		otp: {
			type: String,
		},
	},
	opts
);

/**
 * Before saving hash and salt the password if it has been modified.
 */
userSchema.pre("save", async function (next) {
	try {
		if (this.isModified("password")) {
			const hash = await bcrypt.hash(this.password, 8);
			this.password = hash;
		}
		if (this.isModified("otp")) {
			const otpHash = await bcrypt.hash(this.otp!, 8);
			this.otp = otpHash;
		}

		next();
	} catch (err) {
		next(err as mongoose.Error);
	}
});

/**
 * Compare the hashed password with the password provided.
 */
userSchema.methods.checkPassword = function (password: string) {
	const passwordHash = this.password;
	return new Promise((resolve, reject) => {
		bcrypt.compare(
			password,
			passwordHash,
			(err: Error | undefined, same: boolean) => {
				if (err) {
					return reject(err);
				}
				resolve(same);
			}
		);
	});
};

/**
 * Compare the hashed otp with the otp provided.
 */
userSchema.methods.checkOTP = function (otp: string) {
	const otpHash = this.otp;
	return new Promise((resolve, reject) => {
		bcrypt.compare(otp, otpHash, (err: Error | undefined, same: boolean) => {
			if (err) {
				return reject(err);
			}
			resolve(same);
		});
	});
};

/**
 * @description This is a virtual field, which is not stored in the database, but can be accessed
 * @info Comparing local field (_id) of model 'User' with foreign field (user) of model 'Blog'
 */
userSchema.virtual("blogs", {
	ref: "Blog",
	localField: "_id",
	foreignField: "user",
});

const User: UserModel = mongoose.model<IUser, UserModel>("User", userSchema);

export default User;
