import mongoose, { Document, Model } from "mongoose";
import Joi from "joi";

const Schema = mongoose.Schema;

const opts = { toJSON: { virtuals: true }, timestamps: true };

export interface IBlog extends Document {
	title: string;
	content?: string;
	cover?: string;
	user: string;
}

//

export interface BlogModel extends Model<IBlog> {}

const blogSchema = new Schema(
	{
		title: {
			type: String,
			required: [true, "title is required"],
			trim: true,
			validate: {
				validator: function (v: string) {
					return Joi.string().min(3).max(50).validate(v).error === undefined;
				},
				message: (props: mongoose.ValidatorProps) =>
					`${props.value} is not a valid title!`,
			},
		},
		content: {
			type: String,
			trim: true,
			validate: {
				validator: function (v: string) {
					return Joi.string().validate(v).error === undefined;
				},
				message: (props: mongoose.ValidatorProps) =>
					`${props.value} is not a valid content!`,
			},
		},
		cover: {
			type: String,
			trim: true,
			validate: {
				validator: function (v: string) {
					return Joi.string().uri().validate(v).error === undefined;
				},
				message: (props: mongoose.ValidatorProps) =>
					`${props.value} is not a valid cover URL!`,
			},
		},
		user: {
			type: Schema.Types.ObjectId,
			ref: "User",
			required: true,
		},
	},
	opts
);

const Blog: BlogModel = mongoose.model<IBlog, BlogModel>("Blog", blogSchema);

export default Blog;
