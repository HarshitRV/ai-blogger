console.log("resetPassword.js loaded...");

interface FormDataValue {
	email: FormDataEntryValue | null;
	password: FormDataEntryValue | null;
	confirmPassword: FormDataEntryValue | null;
}

const outputContentDiv = document.getElementById(
	"output-content"
) as HTMLDivElement;
const form = document.getElementById("reset-password") as HTMLFormElement;
const emailInput = document.getElementById("email") as HTMLInputElement;

const receivedEmail: string = emailInput.value;

const emailErrorSpan = document.getElementById(
	"email-error"
) as HTMLSpanElement;
const passwordErrorSpan = document.getElementById(
	"password-error"
) as HTMLSpanElement;
const confirmPasswordErrorSpan = document.getElementById(
	"confirm-password-error"
) as HTMLSpanElement;

const fieldsRequiredErrorSpan = document.getElementById(
	"fields-required-error"
) as HTMLSpanElement;

const showPasswordCheckBox = document.getElementById(
	"show-password"
) as HTMLInputElement;

const passwordInput = document.getElementById("password") as HTMLInputElement;
const confirmPasswordInput = document.getElementById(
	"confirm-password"
) as HTMLInputElement;

const passwordResetSuccessAlert = document.getElementById(
	"password-reset-success"
) as HTMLParagraphElement;
const passwordResetFailureAlert = document.getElementById(
	"password-reset-failure"
) as HTMLParagraphElement;

const emailRegex: RegExp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const passwordRegex: RegExp =
	/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{12,})/;

const errorSpans: Array<HTMLSpanElement> = [
	emailErrorSpan,
	passwordErrorSpan,
	confirmPasswordErrorSpan,
	fieldsRequiredErrorSpan,
];

const urlPathArray: Array<string> = window.location.pathname.split("/");
const token: string = urlPathArray.at(-1)!;
const userId: string = urlPathArray.at(-2)!;

const isValidInput = (): boolean => {
	const formData: FormData = new FormData(form);

	const data: FormDataValue = {
		email: receivedEmail,
		password: formData.get("password"),
		confirmPassword: formData.get("confirm-password"),
	};

	if (!data.email || !data.password || !data.confirmPassword) {
		fieldsRequiredErrorSpan.classList.remove("hidden");
		fieldsRequiredErrorSpan.textContent = "All fields are required";

		return false;
	}

	if (!emailRegex.test(data.email as string)) {
		emailErrorSpan.classList.remove("hidden");
		emailErrorSpan.textContent = "Invalid email";
		return false;
	}

	if (!passwordRegex.test(data.password as string)) {
		passwordErrorSpan.classList.remove("hidden");
		passwordErrorSpan.textContent = "Invalid password";
		return false;
	}

	if (data.password !== data.confirmPassword) {
		confirmPasswordErrorSpan.classList.remove("hidden");
		confirmPasswordErrorSpan.textContent = "Passwords do not match";
		return false;
	}

	return true;
};

showPasswordCheckBox.addEventListener("change", () => {
	if (showPasswordCheckBox.checked) {
		passwordInput.type = "text";
		confirmPasswordInput.type = "text";
	} else {
		passwordInput.type = "password";
		confirmPasswordInput.type = "password";
	}
});

form.addEventListener("input", () => {
	passwordResetFailureAlert.classList.add("hidden");
	errorSpans.forEach((spanElement) => {
		spanElement.classList.add("hidden");
	});
});

form.addEventListener("submit", async (event) => {
	console.log("submitting form...");

	event.preventDefault();

	if (isValidInput()) {
		try {
			const response = await fetch(
				"/api/v1/user/auth/reset-password/new-password",
				{
					method: "post",
					headers: {
						"Content-Type": "application/json",
					},
					body: JSON.stringify({
						email: receivedEmail,
						newPassword: passwordInput.value,
						confirmPassword: confirmPasswordInput.value,
						token,
						userId,
					}),
				}
			);
			if (response.status === 200) {
				const data = await response.json();
				console.log(data);
				outputContentDiv.innerHTML = `<p id="password-reset-success" class="bg-green-100 border-l-4 border-green-500 text-green-700 p-4 my-2" role="alert">
						Password reset successfully
					</p>`;
			} else {
				const data = await response.json();
				console.log(data);
				throw new Error("Password reset failed");
			}
		} catch (error) {
			console.error(error);
			passwordResetFailureAlert.classList.remove("hidden");
		}
	}

	return;
});
