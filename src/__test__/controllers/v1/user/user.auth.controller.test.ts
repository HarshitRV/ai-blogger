import mongoose from "mongoose";
import request, { Response } from "supertest";
import app from "../../../../app";
import connectDb from "../../../../utils/db/connectDb";
import disconnectDb from "../../../../utils/db/disconnectDb";
import { deleteAllCollections } from "../../../../utils/db/utils/utils";

describe("[User Auth API's :- /api/v1/user/auth]", () => {
	const testUserId = new mongoose.Types.ObjectId();
	const baseURL: string = "/api/v1/user/auth";

	/**
	 * DO NOT MODIFY THIS OBJECT
	 */
	const VALID_TEST_USER = {
		username: "TestUser_123",
		email: "testuser@gmail.com",
		password: "testUser@123!",
	} as const;

	const NEW_PASSWORD = "newPassword@123!";

	const INVALID_TEST_USER = {
		username: "TestUser 123",
		email: "invalidtestuser@gmail.com",
		password: "testUser@123!",
	} as const;

	const getResetPasswordLink = async (): Promise<string> => {
		const response: Response = await request(app)
			.post(baseURL + "/reset-password")
			.send({
				email: VALID_TEST_USER.email,
			});

		const { link } = response.body;

		return link;
	};

	beforeAll(async () => {
		await connectDb();
	});

	afterAll(async () => {
		await deleteAllCollections();
		await disconnectDb();
	});

	describe("User signup /register ", () => {
		it("should return 201 status and register a user", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/register")
				.send(VALID_TEST_USER);

			expect(response.status).toBe(201);
			expect(response.body).toMatchObject({
				token: expect.any(String),
				user: {
					username: VALID_TEST_USER.username,
					email: VALID_TEST_USER.email,
				},
			});
		});

		it("should return 400 status and not register a user when request body validation fails", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/register")
				.send(INVALID_TEST_USER);

			expect(response.status).toBe(400);
		});

		it("should return 400 status and not register a user when user already exists", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/register")
				.send(VALID_TEST_USER);

			expect(response.status).toBe(400);
			expect(response.body).toMatchObject({
				message: "User already exists!",
			});
		});
	});

	describe("User login /login ", () => {
		it("should login an existing user using 'email'", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/login")
				.send({
					email: VALID_TEST_USER.email,
					password: VALID_TEST_USER.password,
				});

			expect(response.status).toBe(200);
			expect(response.body).toMatchObject({
				token: expect.any(String),
				user: {
					username: VALID_TEST_USER.username,
					email: VALID_TEST_USER.email,
				},
			});
		});
		it("should login an existing user using 'username'", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/login")
				.send({
					username: VALID_TEST_USER.username,
					password: VALID_TEST_USER.password,
				});

			expect(response.status).toBe(200);
			expect(response.body).toMatchObject({
				token: expect.any(String),
				user: {
					username: VALID_TEST_USER.username,
					email: VALID_TEST_USER.email,
				},
			});
		});
		it("should not login a user if the request body validation fails", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/login")
				.send({
					username: INVALID_TEST_USER.username, // username is invalid
					password: INVALID_TEST_USER.password,
				});

			expect(response.status).toBe(400);
		});

		it("should not login a user if user is not registered", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/login")
				.send({
					email: INVALID_TEST_USER.email, // user with this email never got registered
					password: INVALID_TEST_USER.password,
				});

			expect(response.status).toBe(404);
		});

		it("should not login a user if password is incorrect", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/login")
				.send({
					username: VALID_TEST_USER.username,
					password: "invalidPassword@123!", // incorrect password (it must pass validation though)
				});

			expect(response.status).toBe(401);
		});
	});

	describe("User reset password /reset-password", () => {
		it("should send reset password link to existing user's email, Email was provided", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/reset-password")
				.send({
					email: VALID_TEST_USER.email,
				});

			expect(response.status).toBe(200);
			expect(response.body).toHaveProperty("link");
		});

		it("should send reset password link to existing user's email, Username was provided", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/reset-password")
				.send({
					username: VALID_TEST_USER.username,
				});

			expect(response.status).toBe(200);
			expect(response.body).toHaveProperty("link");
		});

		it("should not send reset password link to non-existing user's, Email was provided", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/reset-password")
				.send({
					email: INVALID_TEST_USER.email,
				});

			expect(response.status).toBe(404);
			expect(response.body).not.toHaveProperty("link");
		});

		it("should render reset password page if user exists", async () => {
			const link = await getResetPasswordLink();

			const urlGetResponse: Response = await request(app).get(
				new URL(link).pathname
			);
			expect(urlGetResponse.status).toBe(200);
		});

		it("should not render reset password if token is invalid", async () => {
			const link = await getResetPasswordLink();

			const url: URL = new URL(link);
			const pathname = url.pathname;
			const token = pathname.split("/").pop()!;

			const temperedToken = token + "tempered";
			const temperedPathname = pathname.replace(token, temperedToken);
			const urlGetResponse: Response = await request(app).get(temperedPathname);

			expect(urlGetResponse.status).toBe(500);
		});

		it("should not render reset password link to non-existing user", async () => {
			const link = await getResetPasswordLink();
			const url: URL = new URL(link);
			const paths = url.pathname.split("/");
			const pathname = url.pathname;
			const userId = paths.at(-2)!;
			const temperedPathname = pathname.replace(userId, testUserId.toString());

			const urlGetResponse: Response = await request(app).get(temperedPathname);

			expect(urlGetResponse.status).toBe(404);
		});

		it("should update the password of the user if token is valid", async () => {
			const link = await getResetPasswordLink();

			const url: URL = new URL(link);
			const pathname = url.pathname;
			const paths = pathname.split("/");
			const userId = paths.at(-2)!;
			const token = paths.at(-1)!;

			const resetPasswordResponse: Response = await request(app)
				.post(baseURL + "/reset-password/new-password")
				.send({
					userId,
					token,
					newPassword: NEW_PASSWORD,
					confirmPassword: NEW_PASSWORD,
					email: VALID_TEST_USER.email,
				});

			expect(resetPasswordResponse.status).toBe(200);
		});

		it("should not login the user with the old password", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/login")
				.send({
					email: VALID_TEST_USER.email,
					password: VALID_TEST_USER.password,
				});

			expect(response.status).toBe(401);
		});

		it("should login the user with the new password", async () => {
			const response: Response = await request(app)
				.post(baseURL + "/login")
				.send({
					email: VALID_TEST_USER.email,
					password: NEW_PASSWORD,
				});

			expect(response.status).toBe(200);
		});
	});
});
