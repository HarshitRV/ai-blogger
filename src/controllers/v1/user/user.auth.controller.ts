import { Request, Response } from "express";
import Joi from "joi";
import jwt from "jsonwebtoken";
import User, { IUser } from "../../../models/user.model";
import asyncErrorHandler from "../../../utils/error_handler/asyncErrorHandler";
import { newToken, verifyToken } from "../../../utils/jwt/jwt";
import { logger } from "../../../utils/logger/logger";

export interface UserRegisterRequestBody {
	username: string;
	email: string;
	password: string;
}

export const passwordRegex = new RegExp(
	"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{12,})"
);

export const usernameRegex = new RegExp(/^[a-zA-Z0-9_]{3,30}$/);

export const userRegisterSchema = Joi.object({
	username: Joi.string().pattern(usernameRegex).required(),
	email: Joi.string().email().required(),
	password: Joi.string().pattern(passwordRegex).required(),
});

export const userLoginSchema = Joi.alternatives().try(
	Joi.object({
		username: Joi.string().pattern(usernameRegex).required(),
		password: Joi.string().pattern(passwordRegex).required(),
	}),
	Joi.object({
		email: Joi.string().email().required(),
		password: Joi.string().pattern(passwordRegex).required(),
	})
);

export const userResetPasswordSchema = Joi.alternatives().try(
	Joi.object({
		email: Joi.string().email().required(),
	}),
	Joi.object({
		username: Joi.string().pattern(usernameRegex).required(),
	})
);
export interface UserResetPasswordRequestBody {
	userId: string;
	token: string;
	newPassword: string;
	confirmPassword: string;
	email: string;
}

export const userResetPasswordTokenSchema = Joi.object({
	userId: Joi.string().required(),
	token: Joi.string().required(),
	newPassword: Joi.string().pattern(passwordRegex).required(),
	confirmPassword: Joi.string().pattern(passwordRegex).required(),
	email: Joi.string().email().required(),
});

export const register = asyncErrorHandler(
	async (req: Request, res: Response) => {
		const { error, value } = userRegisterSchema.validate(req.body);

		if (error) {
			logger.error({
				details: error.details,
			});
			return res.status(400).json({
				details: error.details,
			});
		}

		const { username, email, password } = value as UserRegisterRequestBody;

		const existingUser = await Promise.all([
			User.findOne({ email }),
			User.findOne({ username }),
		]);

		if (existingUser.at(1) || existingUser.at(0)) {
			logger.info({
				message: "User already exists!",
			});
			return res.status(400).json({
				message: "User already exists!",
			});
		}

		const user = new User({
			username,
			email,
			password,
		});

		const token = newToken(user._id);

		await user.save();

		res.status(201).json({
			token,
			user: {
				username,
				email,
			},
		});
	}
);

export const login = asyncErrorHandler(async (req: Request, res: Response) => {
	const { error, value } = userLoginSchema.validate(req.body);

	if (error) {
		logger.error({
			details: error.details,
		});
		return res.status(400).json({
			details: error.details,
		});
	}

	let user: IUser | null = null;
	if (value.username) {
		user = await User.findOne({ username: value.username });
	} else {
		user = await User.findOne({ email: value.email });
	}

	if (!user) {
		logger.info({
			message: "User not found!",
		});
		return res.status(404).json({
			message: "User not found!",
		});
	}

	const match = await user.checkPassword(value.password);

	if (!match) {
		logger.info({
			message: "Invalid credentials!",
		});
		return res.status(401).json({
			message: "Invalid credentials!",
		});
	}

	const token = newToken(user.id);

	return res.status(200).json({
		token,
		user: {
			username: user.username,
			email: user.email,
		},
	});
});

export const generateResetPasswordLink = asyncErrorHandler(
	async (req: Request, res: Response) => {
		const { error, value } = userResetPasswordSchema.validate(req.body);

		if (error) {
			logger.error({
				details: error.details,
			});
			return res.status(400).json({
				details: error.details,
			});
		}

		let user: IUser | null = null;
		if (value.email) {
			user = await User.findOne({ email: value.email });
		} else {
			user = await User.findOne({ username: value.username });
		}

		if (!user) {
			logger.info({
				message: "User not found!",
			});
			return res.status(404).json({
				message: "User not found!",
			});
		}

		const secret = (process.env.JWT_SECRET as string) + user.password;
		const payload = {
			email: user.email,
			id: user.id,
		};
		const token = jwt.sign(payload, secret, { expiresIn: "15m" });
		const baseURL = `${req.protocol}://${req.get("host")}${req.originalUrl}`;
		let link = `${baseURL}${user.id}/${token}`;

		if (!link.includes("/reset-password/")) {
			// believe me including "/" after baseURL does not help
			// sometime you get / after reset-password, sometime you don't 🤷‍♂️
			link = link.replace("/reset-password", "/reset-password/");
		}

		return res.status(200).json({
			link,
			token,
			userId: user.id,
		});

		// TODO: Uncomment this code to send email using Resend
		// const resend = new Resend(process.env.RESEND_API_KEY as string);

		// const sentEmailResponse = await resend.emails.send({
		// 	from: "onboarding@resend.dev",
		// 	to: user.email,
		// 	subject: "Reset Password",
		// 	html: `<div>
		//         <p>Reset your password</p>
		//         <a href="${link}">Click here to reset your password</a>
		//     </div>`,
		// });

		// if (sentEmailResponse.error) {
		// 	logger.error({
		// 		message: sentEmailResponse.error,
		// 	});

		// 	const { name, message } = sentEmailResponse.error;

		// 	return res.status(403).json({
		// 		name,
		// 		message,
		// 	});
		// }

		// res.status(200).json({
		// 	sentMailId: sentEmailResponse.data?.id,
		// });
	}
);

export const renderResetPasswordPage = asyncErrorHandler(
	async (req: Request, res: Response) => {
		const { userId, token } = req.params as { userId: string; token: string };

		const user: IUser | null = await User.findById(userId);

		if (!user) {
			logger.info({
				message: "User not found!",
			});
			return res.status(404).render("reset-password", {
				allowReset: false,
			});
		}

		logger.info({
			message: "User found!",
			user,
			allowReset: true,
		});

		const secret = (process.env.JWT_SECRET as string) + user.password;

		const payload = verifyToken(token, secret) as {
			email: string;
			id: string;
		};

		res.status(200).render("reset-password", {
			allowReset: true,
			email: payload.email,
			userId: payload.id,
		});
	}
);

export const resetPassword = asyncErrorHandler(
	async (req: Request, res: Response) => {
		const { error, value } = userResetPasswordTokenSchema.validate(req.body);

		if (error) {
			logger.error({
				details: error.details,
			});
			return res.status(400).json({
				details: error.details,
			});
		}
		const { email, userId, newPassword, confirmPassword, token } =
			value as UserResetPasswordRequestBody;

		if (newPassword !== confirmPassword) {
			logger.info({
				message: "Passwords do not match!",
			});
			return res.status(400).json({
				message: "Passwords do not match!",
			});
		}

		const user: IUser | null = await User.findOne({ email, _id: userId });

		if (!user) {
			logger.info({
				message: "User not found!",
			});
			return res.status(404).json({
				message: "User not found!",
			});
		}

		const secret = (process.env.JWT_SECRET as string) + user.password;

		verifyToken(token, secret);

		user.password = newPassword;
		await user.save();

		res.status(200).json({
			message: "Password reset successfully!",
		});
	}
);
