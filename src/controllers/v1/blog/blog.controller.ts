import { Request, Response } from "express";
import Joi from "joi";
import Blog, { IBlog } from "../../../models/blog.model";
import asyncErrorHandler from "../../../utils/error_handler/asyncErrorHandler";
import { logger } from "../../../utils/logger/logger";

export const BlogCreateSchema = Joi.object({
	title: Joi.string().max(50).required(),
	content: Joi.string().max(5000).required(),
});

export interface BlogCreateRequestBody {
	title: string;
	content: string;
}

export const createBlog = asyncErrorHandler(
	async (req: Request, res: Response) => {
		const { error, value } = BlogCreateSchema.validate(req.body);
		// const user = req.user;

		if (error) {
			logger.error({
				details: error.details,
			});
			return res.status(400).json({
				details: error.details,
			});
		}

		const { title, content } = value as BlogCreateRequestBody;

		const blog: IBlog = new Blog({
			title,
			content,
		});
	}
);
