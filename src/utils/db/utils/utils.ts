import mongoose from "mongoose";
import { logger } from "../../logger/logger";

export const deleteAllCollections = async () => {
	const collections = Object.keys(mongoose.connection.collections);
	for (const collectionName of collections) {
		const collection = mongoose.connection.collections[collectionName];
		try {
			await collection.drop();
			logger.info(`Dropped ${collectionName} collection.`);
		} catch (error: any) {
			if (error.message === "ns not found") {
				logger.error(`Collection ${collectionName} not found.`);
				return;
			}
			if (
				error.message.includes("a background operation is currently running")
			) {
				logger.error(
					`Could not drop ${collectionName} collection due to background operation. Retrying...`
				);
				await new Promise((resolve) => setTimeout(resolve, 1000));
				await collection.drop();
			} else {
				logger.error(
					`Error occurred when dropping ${collectionName} collection:`,
					error
				);
				throw error;
			}
		}
	}
};
