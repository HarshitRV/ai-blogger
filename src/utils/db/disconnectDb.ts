import mongoose from "mongoose";
import { logger } from "../logger/logger";

const disconnectDb = async () => {
	try {
		logger.info("Disconnecting from db");
		await mongoose.disconnect();
		logger.info("Disconnected from db");
	} catch (e) {
		logger.error("Error disconnecting from db: ", e);
		throw new Error("Error disconnecting from db");
	}
};

export default disconnectDb;
