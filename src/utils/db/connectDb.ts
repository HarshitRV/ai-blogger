import mongoose from "mongoose";
import { logger } from "../logger/logger";

const connectDb = async (uri = process.env.MONGODB_URI as string) => {
	try {
		logger.info("Connecting to aiblogger db");
		await mongoose.connect(uri);
		logger.info("Connected to aiblogger db");
	} catch (e) {
		logger.error("Error connecting to aiblogger db: ", e);
		throw new Error("Error connecting to aiblogger db");
	}
};

export default connectDb;
