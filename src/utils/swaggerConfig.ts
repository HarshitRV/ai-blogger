import swaggerJSDoc from "swagger-jsdoc";

const swaggerDefinition = {
	openapi: "3.0.0",
	info: {
		title: "AI Blogger",
		version: "0.0.1",
		description: "AI Blogger API",
	},
};

const options = {
	swaggerDefinition,
	apis: ["./src/routers/**/*.ts"], // Path to the API routes in your Node.js application
};

const swaggerSpec = swaggerJSDoc(options);

export default swaggerSpec;
