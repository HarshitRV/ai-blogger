import jwt, { JwtPayload } from "jsonwebtoken";
import { Types } from "mongoose";
import AppError from "../error_handler/AppError";
import { logger } from "../logger/logger";

export interface Payload extends JwtPayload {
	id: string;
	iat?: number;
	exp?: number;
}

export const newToken = (id: Types.ObjectId) => {
	return jwt.sign(
		{
			id,
		},
		process.env.JWT_SECRET as string,
		{
			expiresIn: process.env.JWT_EXP,
		}
	);
};

export const verifyToken = <T>(
	token: string,
	secret: string = process.env.JWT_SECRET as string
): T => {
	try {
		const payload = jwt.verify(token, secret);
		return payload as T;
	} catch (error) {
		if (error instanceof jwt.TokenExpiredError) {
			logger.error({
				message: "Token has expired",
			});
			throw new AppError("Token has expired");
		} else if (error instanceof jwt.NotBeforeError) {
			logger.error({
				message: "Token is not active yet",
			});
			throw new AppError("Token is not active yet");
		} else {
			logger.error({
				message: "Token is invalid",
			});
			throw new AppError("Token is invalid");
		}
	}
};
