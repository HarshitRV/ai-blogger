import { Request, Response, NextFunction } from "express";

const asyncErrorHandler = (
	f: (req: Request, res: Response, next?: NextFunction) => Promise<any>
) => {
	return function (req: Request, res: Response, next: NextFunction) {
		f(req, res, next).catch((e) => next(e));
	};
};

export default asyncErrorHandler;
