class AppError extends Error {
	private status: number;
	constructor(message: string = "Something went wrong", status: number = 500) {
		super(message);
		this.status = status;
		this.message = message;
	}

	public getStatus(): number {
		return this.status;
	}

	public getMessage(): string {
		return this.message;
	}
}

export interface ExpressError extends Error {
	status?: number;
	message: string;
	stack?: string;
}

export default AppError;
