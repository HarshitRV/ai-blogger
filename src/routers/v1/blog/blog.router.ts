import { Router, Response } from "express";
import { createBlog } from "../../../controllers/v1/blog/blog.controller";

const blogRouter = Router();

blogRouter
	.route("/create")
	.get((_, res: Response) => {
		res.status(200).send({
			message: "Create new blog route is working!",
		});
	})
	.post(createBlog);

export default blogRouter;
