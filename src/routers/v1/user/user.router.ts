import { Response, Router } from "express";

const userRouter: Router = Router();

userRouter.get("/", (_, res: Response) => {
	res.status(200).send({
		message: "User router is working!",
	});
});

export default userRouter;
