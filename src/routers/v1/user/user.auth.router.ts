import { Request, Response, Router } from "express";
import {
	generateResetPasswordLink,
	login,
	register,
	renderResetPasswordPage,
	resetPassword,
} from "../../../controllers/v1/user/user.auth.controller";

const userAuthRouter: Router = Router();

userAuthRouter.get("/", (_, res: Response) => {
	res.status(200).send({
		message: "User auth router is working!",
	});
});

/**
 * @swagger
 * /api/v1/user/auth/register:
 *   post:
 *     tags:
 *       - UserAuth
 *     summary: Register a new user
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *             example:
 *               username: "testuser"
 *               email: "testuser@example.com"
 *               password: "TestPassword@123!"
 *     responses:
 *       201:
 *         description: User registered successfully.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 token:
 *                   type: string
 *                 user:
 *                   type: object
 *                   properties:
 *                     username:
 *                       type: string
 *                     email:
 *                       type: string
 *               example:
 *                 token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2M2YxNmJjNGRmZDNjOGY3Nzg1OGJlOSIsImlhdCI6MTcxNTQxMDYyMCwiZXhwIjoxNzE2MDE1NDIwfQ.UHG1oT1R2DZMkal8RX3IBJV8I6dhbMeSPRY4NKZWD34"
 *                 user:
 *                   username: "testuser"
 *                   email: "testuser@example.com"
 *       400:
 *         description: Bad request, user registration failed or user already exists.
 */
userAuthRouter
	.route("/register")
	.get((_, res: Response) => {
		res.status(200).json({
			message: "Register user route is working!",
		});
	})
	.post(register);

/**
 * @swagger
 * /api/v1/user/auth/login:
 *   post:
 *     tags:
 *       - UserAuth
 *     summary: Log in a user using email and password or username and password
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *             example:
 *               email: "testuser@example.com"
 *               password: "TestPassword@123!"
 *     responses:
 *       200:
 *         description: User logged in successfully.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 token:
 *                   type: string
 *                 user:
 *                   type: object
 *                   properties:
 *                     username:
 *                       type: string
 *                     email:
 *                       type: string
 *               example:
 *                 token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2M2YxNmJjNGRmZDNjOGY3Nzg1OGJlOSIsImlhdCI6MTcxNTQxMDYyMCwiZXhwIjoxNzE2MDE1NDIwfQ.UHG1oT1R2DZMkal8RX3IBJV8I6dhbMeSPRY4NKZWD34"
 *                 user:
 *                   username: "testuser"
 *                   email: "testuser@example.com"
 *       400:
 *         description: Bad request, user login failed.
 *       401:
 *         description: Unauthorized, invalid credentials.
 *       404:
 *         description: Not found, user not found.
 */
userAuthRouter
	.route("/login")
	.get((_, res: Response) => {
		res.status(200).json({
			message: "Login user route is working!",
		});
	})
	.post(login);

/**
 * @swagger
 * /api/v1/user/auth/reset-password:
 *   post:
 *     tags:
 *       - UserAuth
 *     summary: Generate a reset password link for a user using email or username
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *                 description: Username of the user. This or email is required.
 *               email:
 *                 type: string
 *                 description: Email of the user. This or username is required.
 *             example:
 *               username: "testuser"
 *     responses:
 *       200:
 *         description: Reset password link generated successfully.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 link:
 *                   type: string
 *                 token:
 *                   type: string
 *                 userId:
 *                   type: string
 *               example:
 *                 link: "<hosted url>/api/v1/user/auth/reset-password/123456/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3R1c2VyQGV4YW1wbGUuY29tIiwiaWQiOiIxMjM0NTYiLCJpYXQiOjE2MzI3NzE2MzMsImV4cCI6MTYzMjc3MzQzM30.5BixZG2tRk6B2ydZbQ4LH3FgD-8KD-Wt3aF9Yixadec"
 *                 token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3R1c2VyQGV4YW1wbGUuY29tIiwiaWQiOiIxMjM0NTYiLCJpYXQiOjE2MzI3NzE2MzMsImV4cCI6MTYzMjc3MzQzM30.5BixZG2tRk6B2ydZbQ4LH3FgD-8KD-Wt3aF9Yixadec"
 *                 userId: "123456"
 *       400:
 *         description: Bad request, validation failed.
 *       404:
 *         description: Not found, user not found.
 */
userAuthRouter
	.route("/reset-password")
	.get((req: Request, res: Response) => {
		res.status(200).json({
			message: "Reset password user route is working!",
			baseURL: req.protocol + "://" + req.get("host"),
			originalUrl: req.originalUrl,
		});
	})
	.post(generateResetPasswordLink);

/**
 * @swagger
 * /api/v1/user/auth/reset-password/new-password:
 *   post:
 *     tags:
 *       - UserAuth
 *     summary: Reset a user's password
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 description: Email of the user. This is required.
 *               userId:
 *                 type: string
 *                 description: User ID. This is required.
 *               newPassword:
 *                 type: string
 *                 description: New password. This is required.
 *               confirmPassword:
 *                 type: string
 *                 description: Confirm new password. This is required.
 *               token:
 *                 type: string
 *                 description: Reset password token. This is required.
 *             example:
 *               email: "testuser@example.com"
 *               userId: "123456"
 *               newPassword: "New@123!"
 *               confirmPassword: "NewPassword@123!"
 *               token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3R1c2VyQGV4YW1wbGUuY29tIiwiaWQiOiIxMjM0NTYiLCJpYXQiOjE2MzI3NzE2MzMsImV4cCI6MTYzMjc3MzQzM30.5BixZG2tRk6B2ydZbQ4LH3FgD-8KD-Wt3aF9Yixadec"
 *     responses:
 *       200:
 *         description: Password reset successfully.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *               example:
 *                 message: "Password reset successfully!"
 *       400:
 *         description: Bad request, validation failed or passwords do not match.
 *       404:
 *         description: Not found, user not found.
 */
userAuthRouter
	.route("/reset-password/new-password")
	.get((_, res: Response) => {
		res.status(200).json({
			message: "New password route is working!",
		});
	})
	.post(resetPassword);

/**
 * @swagger
 * /api/v1/user/auth/reset-password/{userId}/{token}:
 *   get:
 *     tags:
 *       - UserAuth
 *     summary: Render the reset password page for a user
 *     parameters:
 *       - in: path
 *         name: userId
 *         required: true
 *         schema:
 *           type: string
 *         description: The user ID
 *       - in: path
 *         name: token
 *         required: true
 *         schema:
 *           type: string
 *         description: The reset password token
 *     responses:
 *       200:
 *         description: Reset password page rendered successfully.
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               description: HTML content of the reset password page
 *       404:
 *         description: Not found, user not found.
 */
userAuthRouter
	.route("/reset-password/:userId/:token")
	.get(renderResetPasswordPage);

export default userAuthRouter;
