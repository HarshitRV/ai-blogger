import { Router, Response } from "express";

const adminRouter = Router();

adminRouter.get("/", (_, res: Response) => {
	res.status(200).send({
		message: "Admin router is working!",
	});
});

export default adminRouter;
