import express, { Express, NextFunction, Request, Response } from "express";
import morgan from "morgan";
import path from "path";
import swaggerUI from "swagger-ui-express";
import adminRouter from "./routers/v1/admin/admin.router";
import blogRouter from "./routers/v1/blog/blog.router";
import userAuthRouter from "./routers/v1/user/user.auth.router";
import userRouter from "./routers/v1/user/user.router";
import { ExpressError } from "./utils/error_handler/AppError";
import { logger } from "./utils/logger/logger";
import swaggerSpec from "./utils/swaggerConfig";

const app: Express = express();

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "/../views"));

app.use(morgan("dev"));
app.use(express.static(path.join(__dirname, "public")));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/api/v1/user", userRouter);
app.use("/api/v1/user/auth", userAuthRouter);
app.use("/api/v1/admin", adminRouter);
app.use("/api/v1/blog", blogRouter);
app.use("/api/v1/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerSpec));

app.route("/").get((_, res: Response) => {
	res.status(200).json({
		message: "Server is running!",
	});
});

app.use(
	(err: ExpressError, req: Request, res: Response, next: NextFunction) => {
		const { status = 500, message = "Something went wrong", stack } = err;

		logger.error({
			status,
			message,
			stack,
		});

		res.status(status).json({
			message,
		});
	}
);

export default app;
