# AI Blogger

# Pre-requisites
1. [Bun 1.1.8](https://bun.sh/)
2. [MongoDB server](https://www.mongodb.com/products/tools/compass) should be running on the local machine

# Setup

1. Clone the repository
2. Install the dependencies

```
bun i
```

3. Create a `.env` `folder` in the root directory and add the following environment variables to `dev.env` `file`

```
JWT_SECRET=<YOUR_JWT_SECRET>
JWT_EXP=7d
MONGODB_URI=mongodb://localhost:27017/aiblogger
```

4. Start the dev server

```
bun run dev
```

# Testing

Run the following command to run the tests

```
bun run test
```

### API Docs are available at the following URL `locally`

```
http://localhost:3005/api/v1/api-docs
```
